select round(avg(e.salary),2),d.department_name
from employees e
join departments d on e.department_id=d.department_id
group by d.department_name
order by round(avg(e.salary),2) desc
fetch first 5 rows only;