select e.manager_id,
m.first_name || ' ' ||m.last_name,
d.department_name,
sum(e.salary) as total_salary
from employees e
join departments d on e.department_id=d.department_id
join employees m on e.manager_id=d.manager_id
group by d.department_name,m.first_name,
m.last_name,e.manager_id;