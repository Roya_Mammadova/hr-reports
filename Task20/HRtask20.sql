select e1.employee_id,
j.job_title,
e1.manager_id
from employees e1
join employees e2 on e1.manager_id=e2.manager_id
and e1.job_id=e2.job_id
join jobs j on e1.job_id=j.job_id
order by j.job_title;

