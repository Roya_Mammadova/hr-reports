select e.employee_id,
e.first_name||' '||e.last_name as employee_name,
e.salary as employee_salary,
m.employee_id as manager_id,
m.first_name||' '||m.last_name as manager_name,
m.salary as manager_salary
from employees e
join employees m on e.manager_id=m.employee_id
where e.salary>m.salary;
