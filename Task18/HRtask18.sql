select sum(e.salary),d.department_name
from employees e
join departments d on e.department_id=d.department_id
group by d.department_name
order by sum(e.salary) desc;